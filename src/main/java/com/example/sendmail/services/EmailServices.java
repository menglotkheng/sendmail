package com.example.sendmail.services;

import com.example.sendmail.entity.Email;

public interface EmailServices {

    void sendMail(Email email);

}
