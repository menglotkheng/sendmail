package com.example.sendmail.services.impl;

import com.example.sendmail.entity.Email;
import com.example.sendmail.services.EmailServices;
import jakarta.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmailServicesImpl implements EmailServices {


    private final JavaMailSender mailSender;

    @Override
    public void sendMail(Email email) {

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setSubject("Welcome " + email.getName());

            String html = "<!doctype html>\n" +
                    "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                    "      xmlns:th=\"http://www.thymeleaf.org\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\"\n" +
                    "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                    "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                    "    <title>Email</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div> <h1>" + email.getSubject() + "</h1></div>\n" +
                    "\n" +
                    "<div> <p>" + email.getBody() + "</p></div>\n" +
                    "\n" +
                    "<div>" + email.getName() + "</div>\n" +
                    "</body>\n" +
                    "</html>\n";
            helper.setText(html, true);
            helper.setTo(email.getEmail());
            mailSender.send(message);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
