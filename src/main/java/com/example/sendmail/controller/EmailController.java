package com.example.sendmail.controller;

import com.example.sendmail.entity.Email;
import com.example.sendmail.services.EmailServices;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/email")
@AllArgsConstructor
public class EmailController {

    private final EmailServices services;

    @PostMapping(path = "/send")
    public ResponseEntity<?> send(@RequestBody Email email) {

        services.sendMail(email);

        return ResponseEntity.ok().body("Email Send Successful ❤️👋");
    }

}
